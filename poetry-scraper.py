import requests
import urllib.request
from bs4 import BeautifulSoup
import os
import errno
import threading

# test_url = "https://www.poetryfoundation.org/poetrymagazine/browse?volume=150&issue=1&page=1"
# save_location = "1.jpg"


# result = requests.get(test_url)
# if result.status_code == 200:
#     soup = BeautifulSoup(result.content, "html.parser")
# else:
#     print("Failed to connect.")
#     exit    

#print(soup.prettify())
#     soup = BeautifulSoup(result.content, "html.parser")

# img = soup.find('div', class_='c-assetStack-media')
# img_link = img.find('img')
# src = img_link['src']
#print(img_link['src'])

volume = 1
issue = 1
starting_page = 1

# url = "https://www.poetryfoundation.org/poetrymagazine/browse?volume=" + str(volume) + "&issue=" + str(issue) + "&page=" + str(page)
# result = requests.get(url)
# soup = BeautifulSoup(result.content, "html.parser")
# img = soup.find('div', class_='c-assetStack-media')
# img_link = img.find('img')
# src = img_link['src']
# r = requests.get(src)
# filename = str(page)+".png"
# os.makedirs(os.path.dirname(filename), exist_ok=True)
# with open(filename, 'wb') as outfile:
#     outfile.write(r.content)

def get_pages(volume, issue):
    thread_page = 1
    url = "https://www.poetryfoundation.org/poetrymagazine/browse?volume=" + str(volume) + "&issue=" + str(issue) + "&page=" + str(thread_page)
    result = requests.get(url)
    while (volume != 193 and issue != 3 and thread_page != 102):
        soup = BeautifulSoup(result.content, "html.parser")
        img = soup.find('div', class_='c-assetStack-media')
        img_link = img.find('img')
        src = img_link['src']
        a = img.find_all('a')[1]
        a_link = a['href']
        issue_var = 'issue=' + str(issue) 
        if issue_var not in a_link:
            break

        filename = str(volume)+"/"+str(issue)+"/"+str(thread_page)+".png"
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        r = requests.get(src)
        with open(filename, 'wb') as outfile:
            outfile.write(r.content)

        print("wrote page: " + str(thread_page) + " of issue: " + str(issue) + " of volume: " + str(volume))
        thread_page=thread_page+1
        url = "https://www.poetryfoundation.org/poetrymagazine/browse?volume=" + str(volume) + "&issue=" + str(issue) + "&page=" + str(thread_page)
        result = requests.get(url)


#193, 3, 101 last one

thread_list = []
url = "https://www.poetryfoundation.org/poetrymagazine/browse?volume=" + str(volume) + "&issue=" + str(issue) + "&page=" + str(starting_page)
result = requests.get(url)
while (result.status_code == 200 and volume < 3):
    print("Starting processing volume: " + str(volume))
    while (result.status_code == 200):
        thread = threading.Thread(target=get_pages, args=[volume, issue]) 
        thread_list.append(thread)
        print("Starting thread for issue: " + str(issue))
        issue=issue+1
        url = "https://www.poetryfoundation.org/poetrymagazine/browse?volume=" + str(volume) + "&issue=" + str(issue) + "&page=" + str(starting_page)
        result = requests.get(url)
    issue=1
    volume=volume+1
    url = "https://www.poetryfoundation.org/poetrymagazine/browse?volume=" + str(volume) + "&issue=" + str(issue) + "&page=" + str(starting_page)
    result = requests.get(url)

for thread in thread_list:
 thread.start()
for thread in thread_list:
 thread.join()


